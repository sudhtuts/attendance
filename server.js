var express = require('express');

// Create our app
var app = express();

app.use(express.static('public'));
app.get('**',(req,res) => {
  res.sendFile(__dirname+'/public/index.html');
});
app.listen(8080, () => {
  console.log('Express server running on localhost:8080');
});