var WatchLiveReloadPlugin = require('webpack-watch-livereload-plugin');
module.exports = {
  entry: "./src/index.js",
  output: {
    path: __dirname,
    filename: "./public/bundle.js"
  },
  resolve: {
    root: __dirname,
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-0']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.png$/,
        loader: "url-loader?limit=100000"
      },
      {
        test: /\.jpg$/,
        loader: "file-loader"
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=image/svg+xml'
      }

    ]
  },
  plugins: [
    new WatchLiveReloadPlugin({
      files: [
        __dirname + '/app/**/**/**.jsx',
        __dirname + '/app/**/**/**.js',
        __dirname + '/app/**/**/**.css',
        __dirname + '/app/**/**/**.scss',
        './app/**/**.jsx',
        __dirname + '/app/**/**.js',
        __dirname + '/app/**/**.css',
        __dirname + '/app/**/**.scss',
        './app/**.jsx'
      ],
      port: 8081
    }),
  ]
};
