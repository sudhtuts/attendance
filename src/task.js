// -- Dummy data task
import { reactLocalStorage } from 'reactjs-localstorage';

export default () => {
  return new Promise(resolve => {
    const data = window.localStorage.getItem('attendanceData');
    if (!data) {
      reactLocalStorage.setObject('attendanceData', true)
      reactLocalStorage.setObject('2017-02-10',
        {
          class: "10",
          section: "A",
          students: [{
            roll_no: "1",
            present: true,
            email: "1@gmail.com"
          },
          {
            roll_no: "2",
            present: true,
            email: "2@gmail.com"
          },
          {
            roll_no: "3",
            present: true,
            email: "3@gmail.com"
          },
          {
            roll_no: "4",
            present: true,
            email: "4@gmail.com"
          },
          {
            roll_no: "5",
            present: true,
            email: "5@gmail.com"
          }
          ]
        }

      );

      reactLocalStorage.setObject('2017-02-09',
        {
          class: "10",
          section: "A",
          students: [{
            roll_no: "1",
            present: true,
            email: "1@gmail.com"
          },
          {
            roll_no: "2",
            present: true,
            email: "2@gmail.com"
          },
          {
            roll_no: "3",
            present: true,
            email: "3@gmail.com"
          },
          {
            roll_no: "4",
            present: true,
            email: "4@gmail.com"
          },
          {
            roll_no: "5",
            present: true,
            email: "5@gmail.com"
          }
          ]
        }
      );

      reactLocalStorage.setObject('2017-02-08',
        {
          class: "10",
          section: "A",
          students: [{
            roll_no: "1",
            present: true,
            email: "1@gmail.com"
          },
          {
            roll_no: "2",
            present: true,
            email: "2@gmail.com"
          },
          {
            roll_no: "3",
            present: true,
            email: "3@gmail.com"
          },
          {
            roll_no: "4",
            present: true,
            email: "4@gmail.com"
          },
          {
            roll_no: "5",
            present: true,
            email: "5@gmail.com"
          }
          ]
        }
      );

      reactLocalStorage.setObject('2017-02-07',
        {
          class: "10",
          section: "A",
          students: [{
            roll_no: "1",
            present: true,
            email: "1@gmail.com"
          },
          {
            roll_no: "2",
            present: false,
            email: "2@gmail.com"
          },
          {
            roll_no: "3",
            present: true,
            email: "3@gmail.com"
          },
          {
            roll_no: "4",
            present: true,
            email: "4@gmail.com"
          },
          {
            roll_no: "5",
            present: true,
            email: "5@gmail.com"
          }
          ]
        }
      );

      reactLocalStorage.setObject('2017-02-06',
        {
          class: "10",
          section: "A",
          students: [{
            roll_no: "1",
            present: true,
            email: "1@gmail.com"
          },
          {
            roll_no: "2",
            present: true,
            email: "2@gmail.com"
          },
          {
            roll_no: "3",
            present: false,
            email: "3@gmail.com"
          },
          {
            roll_no: "4",
            present: true,
            email: "4@gmail.com"
          },
          {
            roll_no: "5",
            present: false,
            email: "5@gmail.com"
          }
          ]
        }
      );
      return resolve(true);
    }
    else {
      return resolve(true);
    }
  });

};