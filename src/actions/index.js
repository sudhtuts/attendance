import { reactLocalStorage } from 'reactjs-localstorage';
import {
    DATE_SELECTED
} from './types';

export function getData(date) {
    return {
        type: DATE_SELECTED,
        payload: reactLocalStorage.getObject(date)
    };
}

export function changeObject(date, object) {
    if(!date || !object) {
      return {
        type: 'JUNK'
      };
    }
    reactLocalStorage.setObject(date, object);
    return (dispatch) => {
        setTimeout(() => {
            dispatch(getData(date));
        }, 75)
    }
}