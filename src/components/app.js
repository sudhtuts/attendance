import React, { Component } from 'react';
import Dashboard from './dashboard';

export default class App extends Component {
  render() {
    return (
      <Dashboard />
    );
  }
}
