import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions/index';

class AttendanceList extends Component {
  constructor(props) {
    super(props);
    this.renderDetails = this.renderDetails.bind(this);
    this.toggleStatus = this.toggleStatus.bind(this);
  }

  toggleStatus(roll,status) {
    let data = this.props.result.data;
    for(let i in data.students) {
      if(data.students[i].roll_no === roll)
      {
        data.students[i].present = status;
      }
    }
    this.props.changeObject(this.props.activeDate,data);
  }

  renderDetails() {
    if ("result" in this.props) {
      if (this.props.result) {
        if(!this.props.result.data)
        {
          return (
            <p>Loading...</p>
          )
        }
        
        let {data} = this.props.result;
        return (
          <table className="table table-hover">
            <thead>
              <tr>
                <th>Class</th>
                <th>Section</th>
                <th>Roll No</th>
                <th>Email</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {data.students.map(student => {
                return (
                <tr key={student.email}>
                  <td>{data.class}</td>
                  <td>{data.section}</td>
                  <td>{student.roll_no}</td>
                  <td>{student.email}</td>
                  <td>{student.present ? "Present" : "Absent"}</td>
                  <td>
                    <a
                      className="btn btn-primary"
                      onClick={evt => this.toggleStatus(student.roll_no, !student.present)}>
                        {student.present ? "Mark as absent" : "Mark as Present"}
                    </a>
                  </td>
                </tr>
                );
              })}
            </tbody>
          </table>
        )
      }
    }
    else {
      return (
        <div>
          Loading...
        </div>
      )
    }
  }

  render() {

    return (
      <div className="row">
        {this.renderDetails()}
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ changeObject: actions.changeObject }, dispatch);
}

export default connect(null,mapDispatchToProps)(AttendanceList);