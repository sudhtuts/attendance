import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import DatePicker from 'react-datepicker';
require('react-datepicker/dist/react-datepicker.css');

import * as actions from '../actions/index';
import AttendanceList from './attendanceList';

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.props.getData(moment('2017-02-06').format('YYYY-MM-DD'));
    this.state = { startDate: moment('2017-02-06') };
  }

  handleChange(date) {
    this.props.getData(moment(date).format('YYYY-MM-DD'));
    this.setState({startDate: date});
  }


  render() {
    return (
      <div className="date-picker-container">
        <div className="row">
          <div className="col-md-4">
            Select The date to get Attendance details...
          </div>
          <div className="col-md-4">
            <DatePicker
              selected={this.state.startDate}
              onChange={this.handleChange.bind(this)} />
          </div>
        </div>
        <br />
        <br />
        <br />
        <AttendanceList {...this.props} activeDate={moment(this.state.startDate).format('YYYY-MM-DD')}/>
        
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    result: state.dashboard
  };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getData: actions.getData }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps )(Dashboard);