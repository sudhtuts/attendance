import {
  DATE_SELECTED
} from '../actions/types';

export default function(state=null, action) {
  switch(action.type) {
    case DATE_SELECTED:
      return { ...state, data: action.payload };
  }

  return state;
};